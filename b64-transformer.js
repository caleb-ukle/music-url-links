require('dotenv').config()
const axios = require('axios')
const sharp = require('sharp')

const NAMESPACE_URL = `${process.env.CF_BASE_URL}/accounts/${process.env.CF_ACCOUNT_ID}/storage/kv/namespaces/${process.env.CF_NAMESPACE_ID}`
const CF_HEADERS = {
  headers: {
    'X-Auth-Email': process.env.CF_AUTH_EMAIL,
    'X-Auth-Key': process.env.CF_AUTH_KEY,
  },
}

/**
 * @param {string} url
 * @return {Promise<AxiosResponse<any>>}
 */
async function download(url) {
  console.log('downloading')
  return axios.get(url, { responseType: 'arraybuffer' })
}

/**
 * @param {Buffer} data
 * @return {Promise<Buffer>}
 */
async function resize(data) {
  console.log('resizing')
  return sharp(data)
    .resize(20)
    .toBuffer()
}

/**
 * @param {Buffer} data
 * @return {string}
 */
function toBase64(data) {
  console.log('making base 64 image')
  return Buffer.from(data).toString('base64')
}


async function transform(record) {

  const { data } = await download(record.img)

  if (!data) {
    throw Error('no image found')
  }

  const resized = await resize(data)

  return {
    ...record,
    b64: toBase64(resized),
  }
}

async function listKeys() {
  return axios.get(`${NAMESPACE_URL}/keys`, { ...CF_HEADERS })
}

async function getKVPair(pairName) {
  return axios.get(`${NAMESPACE_URL}/values/${pairName}`, { ...CF_HEADERS })
    .then(res => {
      return {
        data: {
          ...res.data,
          cf_key_name: pairName,
        },
      }
    })
}

async function bulkUpdateKV(payload) {
  return axios.put(`${NAMESPACE_URL}/bulk`, payload,{...CF_HEADERS})
}


listKeys()
  .then((res) => {
    return Promise.all(res.data.result.map(key => getKVPair(key.name)))
  })
  .then((res) => {
    const nob64Img = res.map(r => r.data)
      .filter(value => !value.b64)
    console.log(`${nob64Img.length} records with b64. fetching`)

    return Promise.all(nob64Img.map(value => transform(value)))
  })
  .then(res => {
    console.log(`updating b64 data for ${res.length}`)
    const payload = res.map(d => {
      const key = d.cf_key_name;
      const value = d;
      delete value.cf_key_name;

      console.log(`building ${value.name}`)
      return {
        key,
        value: JSON.stringify(value)
      }
    })
    return bulkUpdateKV(payload);
  })
  .then(res => {
    console.log('done')
  })
