import { badRequest, buildLinks, buildSearchResults, formatHtml, getAllLinksFormatted, IReplaceable } from './helper'

export async function handleRequest(request: Request): Promise<Response> {

  if (request.method === 'POST') {
    // TODO get data from body and add data to kv store

    return new Response(`TODO save data`, {
      headers: {
        'content-type': 'text/plain',
      },
    })
  }
  const url = new URL(request.url)

  const key = url.searchParams.get('l')
  const isJson = url.searchParams.get('data')
  const search = url.searchParams.get('q')
  const all = url.searchParams.get('all')

  if (search) {
    const searchRes = await buildSearchResults(search)

    return new Response(searchRes, {
      headers: {
        'content-type': 'text/html;charset=utf8',
      },
    })
  }

  if (all) {
    const links = await getAllLinksFormatted()

    return new Response(links, {
      headers: {
        'content-type': 'text/html;charset=utf8',
      },
    })
  }


  if (!key) {
    const {origin} = url;

    return Response.redirect(`${origin}?all=1`, 302)

  }

  const urlJson = await MUSIC_URLS.get(key.toLowerCase())

  if (!urlJson) {
    return badRequest()
  }

  if (isJson) {
    return new Response(urlJson, {
      headers: {
        'content-type': 'application/json',
      },
    })

  }

  const parsed = JSON.parse(urlJson) as IMusicResponse
  const formattedLinks = buildLinks(parsed.links)

  const r: IReplaceable = {
    img: parsed.img,
    name: parsed.name,
    links: formattedLinks,
    url,
    b64: parsed.b64 || '',
  }

  const formattedHtml = formatHtml(r)

  return new Response(formattedHtml, {
    headers: {
      'content-type': 'text/html;charset=utf8',
    },
  })
}


export interface IMusicResponse {
  type: 'Album' | 'Song' | 'Artist' | 'Playlist'
  name: string;
  img: string;
  links: ILink[]
  b64?: string;
}

export interface ILink {
  service: string;
  link: string;
  text?: string;
}
