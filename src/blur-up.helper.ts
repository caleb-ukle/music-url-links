export interface IMarkUpData {
  b64: string;
  caption: string;
  imgUrl: string;
}


export function buildImgMarkup({ b64, caption, imgUrl }: IMarkUpData): string {
  return `<span
class="img-wrapper"
style="padding-bottom: 100%;
       position: relative;
       bottom: 0;
       left: 0;
       display: block;
       background-size: cover;
       background-image: url('data:image/png;base64,${b64}');"
>
<img class="img-sharp artwork"
     alt="${caption}"
     title="${caption}"
     src="data:image/png;base64,${b64}"
     srcset="${imgUrl}"
     sizes="500"   
     loading="lazy"
     style="width: 100%; 
     height: 100%; 
     margin: 0; 
     vertical-align: middle; 
     position: absolute; 
     top: 0; 
     left: 0;"  
/>
</span>`
}
