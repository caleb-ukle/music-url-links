import { ILink } from './handler'
import { defaultB64, DISPLAY_HTML } from './static'
import { buildImgMarkup } from './blur-up.helper'

export function buildLinks(links: ILink[]): string {
  return links.map((l) => {
    const cssName = l.service.split(' ').join('-').toLowerCase()

    const text = l.text || `Listen on ${l.service}`
    return `<a href=${l.link} class="share-link btn ${cssName}">${text}</a>`
  }).join('\n')
}

export function formatHtml(replaceables: IReplaceable): string {
  const imgMarkup = buildImgMarkup({
    imgUrl: replaceables.img,
    caption: `${replaceables.name} Artwork`,
    b64: replaceables.b64 || defaultB64,
  })

  return DISPLAY_HTML
    .replace(/\[\[IMG_URL\]\]/g, replaceables.img)
    .replace(/\[\[NAME\]\]/g, replaceables.name)
    .replace(/\[\[URL\]\]/g, replaceables.url.href)
    .replace(/\[\[MUSIC_NAME\]\]/g, replaceables.name)
    .replace(/\[\[LINKS\]\]/g, replaceables.links)
    .replace(/\[\[IMG_MARKUP\]\]/g, imgMarkup)
}

export function badRequest() {
  return new Response(`<h1>Hey looks like you got a bad link there</h1>`, {
    headers: {
      'content-type': 'text/html;charset=utf8',
    },
  })
}

export async function getAllLinksFormatted(): Promise<string> {
  const items: IListKeys = await MUSIC_URLS.list()

  const castedItems = items.keys.map((k) => {
    return {
      text: k.name,
      link: `/?l=${k.name}`,
      service: 'share',
    }
  })

  const links = buildLinks(castedItems)
  const r: IReplaceable = {
    url: new URL('https://calebukle.com'),
    name: `All Available Links`,
    img: 'https://media.calebukle.com/uploads/music/vinyl-placeholder.jpg',
    links,
  }
  const html = formatHtml(r)
  return html
}


export async function buildSearchResults(term: string): Promise<string> {
  const plusEncoded = term.split(' ').join('+')
  const urlEncoded = encodeURI(term)
  const services: ILink[] = [
    {
      link: `https://listen.tidal.com/search?q=${urlEncoded}`,
      service: 'Tidal',
      text: 'Search on Tidal',
    },
    {
      link: `https://open.spotify.com/search/${urlEncoded}`,
      service: 'Spotify',
      text: 'Search on Spotify',
    },
    {
      link: `https://music.apple.com/us/search?term=${urlEncoded}`,
      service: 'Apple Music',
      text: 'Search on Apple Music',
    },
    {
      link: `https://www.pandora.com/search/${urlEncoded}/all`,
      service: 'Pandora',
      text: 'Search on Pandora',
    },
    {
      link: `https://www.amazon.com/s?k=${plusEncoded}&i=digital-music`,
      service: 'Amazon Music',
      text: 'Search on Amazon Music',
    },
    {
      link: `https://music.youtube.com/search?q=${urlEncoded}`,
      service: 'YouTube Music',
      text: 'Search on YouTube Music',
    },
    {
      link: `https://www.qobuz.com/us-en/search?q=${plusEncoded}`,
      service: 'Qobuz',
      text: 'Search on Qobuz',
    },
  ]

  const links = buildLinks(services)
  const r: IReplaceable = {
    url: new URL('https://calebukle.com'),
    name: `Results for ${term}`,
    img: 'https://media.calebukle.com/uploads/music/vinyl-placeholder.jpg',
    links: `${links} <br> <p class="text_small" style="text-align: center;">Search Hash: ${await hashText(term)}</p>`,
  }
  const html = formatHtml(r)
  return html
}

export async function hashText(text: string): Promise<string> {
  const encoded = new TextEncoder().encode(text)
  const digest = await crypto.subtle.digest({ name: 'SHA-1' }, encoded)
  const hashArray = Array.from(new Uint8Array(digest))
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('')
  return hashHex.slice(0, 7)
}


export interface IReplaceable {
  img: string;
  name: string;
  url: URL;
  links: string;
  b64?: string;
}

interface IListKeys {
  keys: { name: string; expiration?: number }[]
  list_complete: boolean
  cursor: string
}
