export const BAD_REQ = `Oops looks like you have a bad link there`

export const shareable = `<button class="share-link btn share" onclick="shareMe()" >Share this page</button>`

export const DISPLAY_HTML = `<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <meta name="theme-color" content="#7475bd"/>
    <title>[[NAME]]</title>
    <meta name="description" content="[[NAME]]">
    <meta property="og:title" content="Check out this music!">
    <meta property="og:description" content="[[NAME]]">
    <meta property="og:url" content="[[URL]]">
    <meta property="og:type" content="website">
    <meta property="og:image" content="[[IMG_URL]]">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@CU_Galaxy">
    <meta name="twitter:site" content="@CU_Galaxy" />
    <meta name="twitter:title" content="Check out this music!">
    <meta name="twitter:description" content="[[NAME]]">
    <meta name="twitter:image" content="[[IMG_URL]]">
    
    <link rel="canonical" href="[[URL]]"/>
    <link rel="preload" href="https://fonts.googleapis.com/css?family=Rubik:400|Rubik:400">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/icons/icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/icons/icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/icons/icon-152x152.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/icons/icon-192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/icons/icon-96x96.png">

    <!--    applied styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.min.css">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Rubik:400|Rubik:400');

        html, body {
            margin: 0;
            padding: 0;
        }

        * {
            box-sizing: border-box;
        }

        html {
            font-size: 100%;
        }

        /*16px*/

        body {
            background-color: white;
            font-family: 'Rubik', sans-serif;
            font-weight: 400;
            line-height: 1.65;
        }

        p {
            margin-bottom: 1.15rem;
        }

        h1, h2, h3, h4, h5 {
            margin: 2.75rem 0 1.05rem;
            font-family: 'Rubik', sans-serif;
            font-weight: 400;
            line-height: 1.15;
        }

        h1 {
            margin-top: 0;
            font-size: 3.052em;
        }

        h2 {
            font-size: 2.441em;
        }

        .h3,
        h3 {
            font-size: 1.953em;
        }

        h4 {
            font-size: 1.563em;
        }

        h5 {
            font-size: 1.25em;
        }

        small, .text_small {
            font-size: 0.8em;
        }
    </style>
    <style>

        :root {
            --white: #fffffe;
            --mint: #bae8e8;
            --soft-mint: #e3f6f5;
            --dark-gray: #272343;
            --black: #2d334a;
            --yella: #ffd803;
            --head-color: var(--dark-gray);
            --bg-color: var(--white);
            --hl-color: var(--yella);
            --text-color: var(--black);
            --link-color: var(--dark-gray);
            --light-gray-1: #E2E2E9;
            --box-shadow: 0 1px 2px -2px rgba(0, 0, 0, .16), 0 3px 6px 0 rgba(0, 0, 0, .12), 0 5px 12px 4px rgba(0, 0, 0, .09);
            --box-shadow-2: inset 2px 2px 2px 0px rgba(255,255,255,.5),
       7px 7px 20px 0px rgba(0,0,0,.1),
       4px 4px 5px 0px rgba(0,0,0,.1);
            --radial-3: 0%;
            --radial-1: 100%;
            --radial-2: 100%;
            --radial-4: 0%;
            --cyanHSL: 180 100% 50%;
            --pinkHSL: 328 100% 54%;
            --twitter-color: #1DA1F2;
            --facebook-color: rgb(59,89,152);
            --email-color: var(--black);
            --reddit-color: #FF5700;
            --tumblr-color: #001935;
            --pinterest-color: #e60023;
        }

      body {
        background-color: rebeccapurple;
        background-image: radial-gradient( circle at var(--radial-1, 100%) var(--radial-3, 0%), hsl(var(--cyanHSL)), hsl(var(--cyanHSL) / 0%) ), 
        radial-gradient( circle at var(--radial-4, 0%) var(--radial-2, 100%), hsl(var(--pinkHSL)), hsl(var(--pinkHSL) / 0%) );
        background-attachment: fixed;
      }
      
        main {
            display: grid;
            height: 100%;
            place-content: center;
            color: var(--black);
            padding: 1.15rem;
            z-index: 2;
        }
        footer {
        padding: 1.15rem;
        text-align: center;
        }
    </style>
    <style>
        #artwork-container {
          max-width: 500px;
          text-align: center;
        }
        .artwork {
            height: auto;
            width: 100%;
            max-width: 500px;
            transition: all .3s ease-in-out;
            box-shadow: var(--box-shadow-2);

        }
        .artwork:hover,
        .artwork:focus,
        .artwork:active {
            transform: scale(1.04);
            box-shadow: var(--box-shadow-2);
        }
    </style>
 
    <style>
        #links {
        margin-top: 2.25rem;
            display: grid;
            gap: 1.15rem;
        }
        #share-sheet {
         margin-top: 2.25rem;
         display: flex;
         gap: 1.15ch;
         place-content: center;
        }
    
    .share-link {
      text-decoration: none;
      text-align: center;
      color: #fff;
      border-radius: 5px;
      padding: 10px 25px;
      background: transparent;
      cursor: pointer;
      transition: all 0.3s ease;
      position: relative;
      display: inline-block;
       box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
       7px 7px 20px 0px rgba(0,0,0,.1),
       4px 4px 5px 0px rgba(0,0,0,.1);
      outline: none;
    }
    
    .share-btn {
    transition: all .3s ease;
  }
  .share-btn:hover {
    transform: scale(1.1);
    cursor: pointer;
  }
   .btn {
    border: none;
    z-index: 1;
    background-color: #FFF;
    color: var(--black);
   }
   .btn:after {
      position: absolute;
      content: "";
      width: 0;
      height: 100%;
      top: 0;
      right: 0;
      z-index: -1;
      border-radius: 5px;
       box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
       7px 7px 20px 0px rgba(0,0,0,.1),
       4px 4px 5px 0px rgba(0,0,0,.1);
      transition: all 0.3s ease;
   }
    .btn:hover:after {
      left: 0;
      width: 100%;
    }
    .btn:active {
      top: 2px;
    }
       
    .share,
    .share:hover,
    .qobuz,
    .pandora,
    .youtube-music,
    .amazon-music,
    .tidal,
    .spotify,
    .apple-music {
      color: var(--black);
    }
    
    .qobuz:hover,
    .pandora:hover,
    .youtube-music:hover,
    .amazon-music:hover,
    .tidal:hover,
    .spotify:hover,  
    .apple-music:hover {
      /*border: 1px solid #ff2f56;*/
      color: #fff;
    }
    
    .share:after{
        background-color: hsl(var(--cyanHSL));
    }
    .pandora:after {
        background-color: rgb(54, 104, 255);
    }
    .spotify:after {
        background-color: #1DB954;
    }
    .tidal:after  {
        background-color: #000;
    }
    .amazon-music:after {
      background-color: #4300FF;
    }
    .youtube-music:after {
      background-color: #FF0000;
    }
    .apple-music:after { 
      background-color: #ff2f56;
    }
    .qobuz:after {
      background-color: #333;
    }
    </style>
</head>
<body>
<!--<div class="background"></div>-->
<main>
    <section id="artwork-container">
        <h1>[[NAME]]</h1>
        
        [[IMG_MARKUP]]
</span>
        
    </section>
    <section id="links">
    [[LINKS]]
<!--    <a href="google.com" class="share-link btn btn-15" >Listen on Amazon Music</a>-->
    </section>
    
    <section id="share-sheet">
     <a class="share-btn clean_link twitter"
     href="https://twitter.com/intent/tweet?text=Check%20out%20these%20tunes%21&url=[[URL]]"
     rel="noreferrer nofollow noopener"
     target="_blank"
     aria-label="Share on Twitter"
  >
    <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="var(--twitter-color)" viewBox="0 0 448 512"
         role="img" aria-label="clickable twitter icon"
    >
      <path
        d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z"/>
    </svg>
  </a>
  <a class="share-btn clean_link facebook"
     href="https://www.facebook.com/sharer/sharer.php?u=[[URL]]"
     aria-label="Share on Facebook"
  >
    <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="var(--facebook-color)" viewBox="0 0 448 512"
         role="img" aria-label="clickable facebook icon"
    >
      <path
        d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"/>
    </svg>
  </a>
  
  <a class="share-btn clean_link tumblr"
     href="http://tumblr.com/widgets/share/tool?canonicalUrl=[[URL]]&posttype=link&title=[[NAME]]&caption=Check%20out%20these%20tunes%21"
     aria-label="Share on tumblr"
  >
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="36" height="36" role="img" aria-label="clickable tumblr icon" fill="var(--tumblr-color)">
  <path d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-82.3 364.2c-8.5 9.1-31.2 19.8-60.9 19.8-75.5 0-91.9-55.5-91.9-87.9v-90h-29.7c-3.4 0-6.2-2.8-6.2-6.2v-42.5c0-4.5 2.8-8.5 7.1-10 38.8-13.7 50.9-47.5 52.7-73.2.5-6.9 4.1-10.2 10-10.2h44.3c3.4 0 6.2 2.8 6.2 6.2v72h51.9c3.4 0 6.2 2.8 6.2 6.2v51.1c0 3.4-2.8 6.2-6.2 6.2h-52.1V321c0 21.4 14.8 33.5 42.5 22.4 3-1.2 5.6-2 8-1.4 2.2.5 3.6 2.1 4.6 4.9l13.8 40.2c1 3.2 2 6.7-.3 9.1z"/>
</svg>

  </a>
  
  <a class="share-btn clean_link pinterest"
     href="https://pinterest.com/pin/create/button/?url=[[URL]]&media=[[IMG_URL]]&description=Check%20out%20these%20tunes%21"
     aria-label="Share on Pinterest"
  >
   <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"
   fill="var(--pinterest-color)"
   width="36" height="36" role="img" aria-label="clickable Pinterest icon"
   >
  <path d="M448 80v352c0 26.5-21.5 48-48 48H154.4c9.8-16.4 22.4-40 27.4-59.3 3-11.5 15.3-58.4 15.3-58.4 8 15.3 31.4 28.2 56.3 28.2 74.1 0 127.4-68.1 127.4-152.7 0-81.1-66.2-141.8-151.4-141.8-106 0-162.2 71.1-162.2 148.6 0 36 19.2 80.8 49.8 95.1 4.7 2.2 7.1 1.2 8.2-3.3.8-3.4 5-20.1 6.8-27.8.6-2.5.3-4.6-1.7-7-10.1-12.3-18.3-34.9-18.3-56 0-54.2 41-106.6 110.9-106.6 60.3 0 102.6 41.1 102.6 99.9 0 66.4-33.5 112.4-77.2 112.4-24.1 0-42.1-19.9-36.4-44.4 6.9-29.2 20.3-60.7 20.3-81.8 0-53-75.5-45.7-75.5 25 0 21.7 7.3 36.5 7.3 36.5-31.4 132.8-36.1 134.5-29.6 192.6l2.2.8H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48z"/>
</svg>

  </a>
  
  <a class="share-btn clean_link reddit"
     href="http://www.reddit.com/submit?url=[[URL]]&title=[[NAME]]"
     aria-label="Share on reddit"
  >
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" fill="var(--reddit-color)" 
    width="36" height="36" role="img" aria-label="clickable reddit icon">
  <path d="M283.2 345.5c2.7 2.7 2.7 6.8 0 9.2-24.5 24.5-93.8 24.6-118.4 0-2.7-2.4-2.7-6.5 0-9.2 2.4-2.4 6.5-2.4 8.9 0 18.7 19.2 81 19.6 100.5 0 2.4-2.3 6.6-2.3 9 0zm-91.3-53.8c0-14.9-11.9-26.8-26.5-26.8-14.9 0-26.8 11.9-26.8 26.8 0 14.6 11.9 26.5 26.8 26.5 14.6 0 26.5-11.9 26.5-26.5zm90.7-26.8c-14.6 0-26.5 11.9-26.5 26.8 0 14.6 11.9 26.5 26.5 26.5 14.9 0 26.8-11.9 26.8-26.5 0-14.9-11.9-26.8-26.8-26.8zM448 80v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48zm-99.7 140.6c-10.1 0-19 4.2-25.6 10.7-24.1-16.7-56.5-27.4-92.5-28.6l18.7-84.2 59.5 13.4c0 14.6 11.9 26.5 26.5 26.5 14.9 0 26.8-12.2 26.8-26.8 0-14.6-11.9-26.8-26.8-26.8-10.4 0-19.3 6.2-23.8 14.9l-65.7-14.6c-3.3-.9-6.5 1.5-7.4 4.8l-20.5 92.8c-35.7 1.5-67.8 12.2-91.9 28.9-6.5-6.8-15.8-11-25.9-11-37.5 0-49.8 50.4-15.5 67.5-1.2 5.4-1.8 11-1.8 16.7 0 56.5 63.7 102.3 141.9 102.3 78.5 0 142.2-45.8 142.2-102.3 0-5.7-.6-11.6-2.1-17 33.6-17.2 21.2-67.2-16.1-67.2z"/>
</svg>

  </a>

  <a class="share-btn clean_link email"
     href="mailto:?subject=Check%20out%20these%20tunes%21&body=[[URL]]"
     rel="noreferrer nofollow noopener"
     target="_blank"
     aria-label="Share via Email">
     <svg fill="var(--email-color)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="36" height="36" role="img" aria-label="clickable facebook icon">
     <path d="M400 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zM178.117 262.104C87.429 196.287 88.353 196.121 64 177.167V152c0-13.255 10.745-24 24-24h272c13.255 0 24 10.745 24 24v25.167c-24.371 18.969-23.434 19.124-114.117 84.938-10.5 7.655-31.392 26.12-45.883 25.894-14.503.218-35.367-18.227-45.883-25.895zM384 217.775V360c0 13.255-10.745 24-24 24H88c-13.255 0-24-10.745-24-24V217.775c13.958 10.794 33.329 25.236 95.303 70.214 14.162 10.341 37.975 32.145 64.694 32.01 26.887.134 51.037-22.041 64.72-32.025 61.958-44.965 81.325-59.406 95.283-70.199z"/>
     </svg>
  </a>

</section>
</main>


<footer>
    <p>
        Made with ❤️ (and a small amount of <a href="https://calebukle.com/blog/building-a-music-link-sharer-with-cloudflare-workers">Psh, I could do that</a>) by <a href="https://calebukle.com">Caleb Ukle</a>.
    </p>
    <p>
      <a href="/">View all links</a>
    </p>
</footer>
</body>
<script data-blurup>
  const imageWrappers = document.querySelectorAll('.img-wrapper')
  for (let i = 0; i < imageWrappers.length; i++) {
    const imgWrap = imageWrappers[i];
    const imgEl = imgWrap.querySelector('img');
    const onImageComplete = () => {
      imgEl.style.opacity = 1;
      imgEl.style.filter = null;
      imgEl.style.color = 'inherit';
      // imgEl.style.boxShadow = 'inset 0 0 0 400px white'
      imgEl.removeEventListener('load', onImageLoad)
      imgEl.removeEventListener('error', onImageComplete)
    }
    const onImageLoad = () => {
      imgEl.style.transition = 'opacity .4s cubic-bezier(0.4, 0.0, 0.2, 1)';
      onImageComplete()
    }
    imgEl.style.opacity = 0;
    imgEl.style.filter = 'blur(10px)';
    imgEl.style.transform = 'scale(1)';
    imgEl.addEventListener('load', onImageLoad)
    imgEl.addEventListener('error', onImageComplete)
    if (imgEl.complete) {
      onImageComplete()
    }
  }
</script>
  <script>
    let shareSheet = document.querySelector('#share-sheet')
    if(navigator.share) {
      shareSheet.innerHTML = '${shareable}'
    } else {
      console.debug('Unable to use native share sheet')
    }
    
   async function shareMe() {  
      if (navigator.share) {
          try {
            await navigator.share({
              url: location.href
            })
          } catch (e) {
            if (e && !/cancellation/.test(e.message)) {
              console.error(e)
              alert('Unable to share. Unknown error. ' +e.toString() + ' Try Refreshing.')
            }
          }
        }
    }
  </script>
  
  <script>
  function isArrayMatch(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;
    
    console.log('checking elements')
    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    
    return true;
  }
  
  let dt = Date.now()
  let seq = [
    "ArrowUp",
    "ArrowUp",
    "ArrowDown",
    "ArrowDown",
    "ArrowLeft",
    "ArrowRight",
    "ArrowLeft",
    "ArrowRight",
    "KeyB",
    "KeyA"
  ]
  let uSeq = []
  document.addEventListener('keydown', ($e) => {
    if(!seq.includes($e.code)) {
      return;
    }
    
    if(Date.now() - dt > 1000) {
      uSeq = []
      console.log('reset')
    }
    
    uSeq.push($e.code)
    if(isArrayMatch(uSeq, seq)) {
      alert('Beep Boop, secret message')
    } 
    dt = Date.now()
  })
  </script>
</html>`


export const defaultImageMarkUp = `<span class='img-wrapper'
      style="
padding-bottom: 100%;
position: relative;
bottom: 0;
left: 0;
display: block;
background-size: cover;
background-image: url('data:image/png;base64, /9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAcGBQj/xAArEAABAwIEAwgDAAAAAAAAAAABAgMEAAUGERMhElFhBxQiMUFicXKBkaH/xAAYAQACAwAAAAAAAAAAAAAAAAADBAEFBv/EABsRAAICAwEAAAAAAAAAAAAAAAECAAMRITGR/9oADAMBAAIRAxEAPwCSYHTBk3GXMuMBF1EU8S4veNEqTzB9f7VDufbbY4uHXrVYLTNsj2YHAhttSQQd9wcyculYe/YQew4/NeTBckNuZ6TqUk6YPmNjn+amctUnVOsV5+9O9KVtXeujkQr1vU+T2dm8YnnXC4vSVzpiis7eIjb4FKz/ABO8j+qUYIoGAJBusOyx9ntedHadZdStAKUAZCpriO0QXHSVx2yfqKUrKgkNqXw5MXItEMOqAaAHSlKU8GOOwJUT/9k=');"
>
<img src="data:image/png;base64,/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAcGBQj/xAArEAABAwIEAwgDAAAAAAAAAAABAgMEAAUGERMhElFhBxQiMUFicXKBkaH/xAAYAQACAwAAAAAAAAAAAAAAAAADBAEFBv/EABsRAAICAwEAAAAAAAAAAAAAAAECAAMRITGR/9oADAMBAAIRAxEAPwCSYHTBk3GXMuMBF1EU8S4veNEqTzB9f7VDufbbY4uHXrVYLTNsj2YHAhttSQQd9wcyculYe/YQew4/NeTBckNuZ6TqUk6YPmNjn+amctUnVOsV5+9O9KVtXeujkQr1vU+T2dm8YnnXC4vSVzpiis7eIjb4FKz/ABO8j+qUYIoGAJBusOyx9ntedHadZdStAKUAZCpriO0QXHSVx2yfqKUrKgkNqXw5MXItEMOqAaAHSlKU8GOOwJUT/9k="
     style="width: 100%; height: 100%; margin: 0; vertical-align: middle; position: absolute; top: 0; left: 0;"
     srcset="[[IMG_URL]]" alt="[[MUSIC_NAME]] artwork" class="img-sharp artwork">`


export const defaultB64 = `/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAUABQDASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAcGBQj/xAArEAABAwIEAwgDAAAAAAAAAAABAgMEAAUGERMhElFhBxQiMUFicXKBkaH/xAAYAQACAwAAAAAAAAAAAAAAAAADBAEFBv/EABsRAAICAwEAAAAAAAAAAAAAAAECAAMRITGR/9oADAMBAAIRAxEAPwCSYHTBk3GXMuMBF1EU8S4veNEqTzB9f7VDufbbY4uHXrVYLTNsj2YHAhttSQQd9wcyculYe/YQew4/NeTBckNuZ6TqUk6YPmNjn+amctUnVOsV5+9O9KVtXeujkQr1vU+T2dm8YnnXC4vSVzpiis7eIjb4FKz/ABO8j+qUYIoGAJBusOyx9ntedHadZdStAKUAZCpriO0QXHSVx2yfqKUrKgkNqXw5MXItEMOqAaAHSlKU8GOOwJUT/9k=`
